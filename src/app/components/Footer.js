import styles from "./Footer.module.css";

export default function Footer() {
  return (
    <footer className={`${styles.footer} text-center text-lg-start`}>
      <div className={`text-center p-3 ${styles.text}`}>
        © 2022 - 2023 Copyright: GameStation / Contact: US
      </div>
    </footer>
  );
}
